package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class GS {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public GS() {
    }
    
    public ArrayList<Song> getGSSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                        	          	 //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Dont Speak", "No Doubt");         			   		 //Create a song
         Song track2 = new Song("Its My Life", "No Doubt");    					     //Create another song
         Song track3 = new Song("Just a Girl", "No Doubt");      				  	 //Create another song
         Song track4 = new Song("Hey Baby", "No Doubt");      					  	 //Create another song
         Song track5 = new Song("Underneath it All", "No Doubt");        			 //Create another song
         
         this.albumTracks.add(track1);                                          //Add the first song to song list for No Doubt
         this.albumTracks.add(track2);											//Add the second song to song list for No Doubt
         this.albumTracks.add(track3);                                          //Add the third song to song list for No Doubt
         this.albumTracks.add(track4);                                          //Add the fourth song to song list for No Doubt 
         this.albumTracks.add(track5);                                          //Add the fifth song to song list for No Doubt
         																		 
         return albumTracks;                                                    //Return the songs for No Doubt in the form of an ArrayList
    }
}
