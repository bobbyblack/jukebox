package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Weezer {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Weezer() {
    }
    
    public ArrayList<Song> getWeezerSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                        	          			 //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Island in the Sun", "Weezer");         			   		 //Create a song
         Song track2 = new Song("Say it Ain't So", "Weezer");    					     	//Create another song
         Song track3 = new Song("Buddy Holly", "Weezer");      				  	 			//Create another song
         Song track4 = new Song("Undone", "Weezer");      					  				 //Create another song
         Song track5 = new Song("Hash Pipe", "Weezer");        								 //Create another song
         
         this.albumTracks.add(track1);                                          //Add the first song to song list for Weezer
         this.albumTracks.add(track2);											//Add the second song to song list for Weezer
         this.albumTracks.add(track3);                                          //Add the third song to song list for Weezer
         this.albumTracks.add(track4);                                          //Add the fourth song to song list for Weezer 
         this.albumTracks.add(track5);                                          //Add the fifth song to song list for No Weezer
         																		 
         return albumTracks;                                                    //Return the songs for Weezer in the form of an ArrayList
    }
}
